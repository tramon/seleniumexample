package com.epamlab.controller;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;

import java.io.File;

public class XMLCredentialsReader {

    private String url;
    private String login;
    private String password;
    private String chromeDriverName;
    private String chromeDriverPath;
    private String fireFoxDriverName;
    private String fireFoxDriverPath;

    private final String DRIVER_NAME_STRING = "drivername";
    private final String DRIVER_PATH_STRING = "driverpath";
    private final String URL_STRING = "url";
    private final String LOGIN_STRING = "login";
    private final String PASSWORD_STRING = "password";

    private final int FIRST_ELEMENT_IN_XML = 0;
    private final int SECOND_ELEMENT_IN_XML = 1;

    public String getChromeDriverName() {
        return chromeDriverName;
    }

    public String getChromeDriverPath() {
        return chromeDriverPath;
    }

    public String getFireFoxDriverName() {
        return fireFoxDriverName;
    }

    public String getFireFoxDriverPath() {
        return fireFoxDriverPath;
    }

    private final String CREDENTIALS_PATH = "src/main/resources/Credentials.xml";
    private final String DRIVER_CONFIG_PATH= "src/main/resources/SeleniumConfig.xml";

    public XMLCredentialsReader() {
        readFromXML();
    }

    private String readAttribute(Document document, String attributeName, int numberOfElement) {
        String result = "";
        result = document.getElementsByTagName(attributeName).item(numberOfElement).getTextContent();
        return result;
    }

    private void readFromXML() {
        try {
            File xmlFile = new File(CREDENTIALS_PATH);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);

            doc.getDocumentElement().normalize();

            url = readAttribute(doc, URL_STRING, 0);
            login = readAttribute(doc, LOGIN_STRING, 0);
            password = readAttribute(doc, PASSWORD_STRING, 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            File xmlFile = new File(DRIVER_CONFIG_PATH);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);

            doc.getDocumentElement().normalize();

            chromeDriverName = readAttribute(doc, DRIVER_NAME_STRING, FIRST_ELEMENT_IN_XML);
            chromeDriverPath = readAttribute(doc, DRIVER_PATH_STRING, FIRST_ELEMENT_IN_XML);

            fireFoxDriverName = readAttribute(doc, DRIVER_NAME_STRING, SECOND_ELEMENT_IN_XML);
            fireFoxDriverPath = readAttribute(doc, DRIVER_PATH_STRING, SECOND_ELEMENT_IN_XML);

        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    public String toString () {
        return "url: " + url + "\nlogin: " + login + "\npassword: " + password;
    }

    public String getUrl() {
        return url;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
