package com.epamlab.view;

import com.epamlab.controller.XMLCredentialsReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by tramon on 18.12.2016.
 */
public class Authorize {
    private String url;
    private String login;
    private String password;
    private String chromeDriverName;
    private String chromeDriverPath;
    private String fireFoxDriverName;
    private String fireFoxDriverPath;

    public final String SEARCH_RESULTS = "facebook";
    public final String PAGE_TITLE = " page title is: ";
    public final String EMAIL = "email";
    public final String PASSWORD = "pass";

    private XMLCredentialsReader xmlCredentialsReader;

    public Authorize() {
        xmlCredentialsReader = new XMLCredentialsReader();
        url = xmlCredentialsReader.getUrl();
        login = xmlCredentialsReader.getLogin();
        password = xmlCredentialsReader.getPassword();
        chromeDriverName = xmlCredentialsReader.getChromeDriverName();
        chromeDriverPath = xmlCredentialsReader.getChromeDriverPath();
        fireFoxDriverName = xmlCredentialsReader.getFireFoxDriverName();
        fireFoxDriverPath = xmlCredentialsReader.getFireFoxDriverPath();
    }

    public static void main(String[] args) {
        Authorize authorize = new Authorize();
        authorize.runChromeTest();
        authorize.runFireFoxTest();

    }

    public void runChromeTest() {
        System.setProperty(chromeDriverName, chromeDriverPath);
        WebDriver driver = new ChromeDriver();

        driver.navigate().to(url);
        WebElement element = driver.findElement(By.name(EMAIL));
        element.sendKeys(login);
        WebElement element2 = driver.findElement(By.name(PASSWORD));
        element2.sendKeys(password);
        element.submit();

        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getTitle().toLowerCase().startsWith(SEARCH_RESULTS);
            }
        });
        System.out.println(driver.getClass().getSimpleName() +  PAGE_TITLE + driver.getTitle());
        //driver.quit();

    }

    public void runFireFoxTest() {
        System.setProperty(fireFoxDriverName, fireFoxDriverPath);
        WebDriver driver = new FirefoxDriver();

        driver.navigate().to(url);
        WebElement element = driver.findElement(By.name(EMAIL));
        element.sendKeys(login);
        WebElement element2 = driver.findElement(By.name(PASSWORD));
        element2.sendKeys(password);
        element.submit();

        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getTitle().toLowerCase().startsWith(SEARCH_RESULTS);
            }
        });
        System.out.println(driver.getClass().getSimpleName() +  PAGE_TITLE + driver.getTitle());
        //driver.quit();

    }


}





